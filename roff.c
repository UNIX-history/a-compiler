/* roff - text justifier		Author: George L. Sicherman */

/*
 *	roff - C version.
 *	the Colonel.  19 May 1983.
 *
 *	Copyright 1983 by G. L. Sicherman.
 *	You may use and alter this software freely for noncommercial ends
 *	so long as you leave this message alone.
 *
 *	Fix by Tim Maroney, 31 Dec 1984.
 *	.hc implemented, 8 Feb 1985.
 *	Fix to hyphenating with underlining, 12 Feb 1985.
 *	Fixes to long-line hang and .bp by Dave Tutelman, 30 Mar 1985.
 *	Fix to centering valve with long input lines, 4 May 1987.
 *
 *	Functionality removed and converted to a simpler C dialect so that
 *	it might be runnable on PDP-7 Unix - Warren Toomey, April 2016.
 */

#ifdef A_LANG
#define EOF 4
void strcpy();
int strlen();
#else
#include <sys/types.h>
#include <sys/stat.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#endif

#define MAXLENGTH 90
#define MAXTITLE 90
int UNDERL=0200;

char tempbuf[10];       /* Used by itoa() */
int sflag; int hflag; int startpage; int stoppage;
char holdword[MAXLENGTH];
char *holdp;
char assyline[MAXLENGTH];
int assylen;
char ehead[MAXTITLE]; char efoot[MAXTITLE];
char ohead[MAXTITLE]; char ofoot[MAXTITLE];
int depth;
int adjtoggle;
int isrequest = 0;
int o_cc = '.';			/* CONTROL CHARACTER */
int o_hc = -1;			/* HYPHENATION CHARACTER */
int o_tc = ' ';			/* TABULATION CHARACTER */
int o_in = 0;			/* INDENT SIZE */
int o_ix = -1;			/* NEXT INDENT SIZE */
int o_ta[] = {
	9, 17, 25, 33, 41, 49, 57, 65, 73, 81, 89, 97, 105,
	113, 121, 129, 137, 145, 153, 161};
int n_ta = 20;			/* #TAB STOPS */
int o_ll = 65; int o_ad = 1; int o_po = 0; int o_ls = 1; int o_ig = 0;
int o_fi = 1;
int o_pl = 66; int o_hx = 0; int o_bl = 0; int o_sp = 0;
int o_ce = 0; int o_m1 = 2; int o_m2 = 2; int o_m3 = 1; int o_m4 = 3;
int o_ul = 0;
int o_li = 0; int o_n1 = 0; int o_n2 = 0; int o_bp = -1; int o_hy = 1;
int o_ni = 1;			/* LINE-NUMBER INDENT */
int o_nn = 0;			/* #LINES TO SUPPRESS NUMBERING */
int o_ti = -1;			/* TEMPORARY INDENT */
int page_no = -1;
int line_no = 9999;
int n_outwords;
int TXTLEN;
char c;				/* LAST CHAR READ */
char cachec=0;			/* One char cache used by suck() */

int col_no=0;			/* Used by spit() */
int n_blanks=0;			/* Used by spit() */

int main();
void readfile();
int readline();
void bumpword();
void dehyph();
int reallen();
void tabulate();
int readreq();
int tread();
void nread();
void do_ta();
int skipsp();
void writebreak();
void blankline();
void writeline();
void fillline();
void insrt();
void newpage();
void beginpage();
void endpage();
void writetitle();
char *pgform();
#ifdef A_LANG
int titlen();
void spit();
#else
int titlen(char *t, char c, int k);
void spit(char c);
void printn(int n);
#endif
void spits();
int suck();
int isspace();
int isalnum();
int isdigit();
int islegal();
char *itoa();

int main()
{

  TXTLEN = o_pl-o_m1-o_m2-o_m3-o_m4 - 2;
  assylen = 0;
  assyline[0] = 0;
  readfile();

  writebreak();
  endpage();
  return(0);
}


void readfile()
{
  while (readline()) {
	if (isrequest) continue;
	if (o_ce || !o_fi) {
		if (assylen)
			writeline(0, 1);
		else
			blankline();
		if (o_ce) o_ce=o_ce - 1;
	}
  }
}

int readline()
{
  int startline; int doingword;
  isrequest = 0;
  startline = 1;
  doingword = 0;
  c = suck();
  if (c == '\n') {
	o_sp = 1;
	writebreak();
	goto out;
  } else if (isspace(c))
	writebreak();
  while(1) {
	if (c == EOF) {
		if (doingword) bumpword();
		break;
	}
	if (c != o_cc && o_ig) {
		while (c != '\n' && c != EOF) c = suck();
		break;
	}
	if (isspace(c) && !doingword) {
		startline = 0;
		if (c==' ') {
			assyline[assylen] = ' ';
			assylen= assylen + 1;
		}
		if (c=='\t') {
		    	tabulate();
		}
		if (c=='\n')	goto out;
		c = suck();
		continue;
	}
	if (isspace(c) && doingword) {
		bumpword();
		if (c == '\t')
			tabulate();
		else if (assylen) {
			assyline[assylen] = ' ';
			assylen= assylen + 1;
		}
		doingword = 0;
		if (c == '\n') break;
	}
	if (!isspace(c)) {
		if (doingword) {
			if (o_ul)
				*holdp = c | UNDERL;
			else
				*holdp = c;
			holdp= holdp + 1;
		} else if (startline && c == o_cc && !o_li) {
			isrequest = 1;
			return readreq();
		} else {
			doingword = 1;
			holdp = holdword;
			if (o_ul)
				*holdp = c | UNDERL;
			else
				*holdp = c;
			holdp= holdp + 1;
		}
	}
	startline = 0;
	c = suck();
  }
out:
  if (o_ul) o_ul=o_ul - 1;
  if (o_li) o_li= o_li - 1;
  return c != EOF;
}

/*
 *	bumpword - add word to current line.
 */

void bumpword()
{
  *holdp = '\0';
/*
 *	Tutelman's fix #1, modified by the Colonel.
 */
  if (!o_fi || o_ce) goto giveup;
/*
 *	We use a while-loop in case of ridiculously long words with
 *	multiple hyphenation indicators.
 */
  if (assylen + reallen(holdword) > o_ll - o_in) {
	if (!o_hy)
		writeline(o_ad, 0);
	else
		while (assylen + reallen(holdword) > o_ll - o_in) {

/*
 *	If no hyphenation marks, give up.
 *	Let somebody else implement it.
 */
				writeline(o_ad, 0);
				goto giveup;
		}		/* while */
  }
giveup:
/*
 *	remove hyphenation marks, even if hyphenation is disabled.
 */
  if (o_hc) dehyph(holdword);
  strcpy(&assyline[assylen], holdword);
  assylen = assylen + strlen(holdword);
  holdp = holdword;
}

/*
 *	dehyph - remove hyphenation marks.
 */

void dehyph(char *s)
{
  char *t;
  for (t = s; *s; s++)
	if ((*s & ~UNDERL) != o_hc) {
		*t = *s; t++;
	}
  *t = 0;
}

/*
 *	reallen - length of a word, excluding hyphenation marks.
 */

int reallen(char *s)
{
  int n;
  n = 0;
  while (*s) {
	n = n + (o_hc != (~UNDERL & *s)); s++;
  }
  return n;
}

void tabulate()
{
  int j;
  for (j = 0; j < n_ta; j++)
	if (o_ta[j] - 1 > assylen + o_in) {
		for (; assylen + o_in < o_ta[j] - 1; assylen=assylen+1)
			assyline[assylen] = o_tc;
		return;
	}

  /* NO TAB STOPS REMAIN */
  assyline[assylen] = o_tc; assylen=assylen+1;
}

int readreq()
{
  int r;
  if (skipsp()) return c != EOF;
  c = suck();
  if (c == EOF || c == '\n') return c != EOF;
  if (c == '.') {
	o_ig = 0;
	do
		(c = suck());
	while (c != EOF && c != '\n');
	return c != EOF;
  }
  if (o_ig) {
	while (c != EOF && c != '\n') c = suck();
	return c != EOF;
  }
  r = c << 9;
  c = suck();
  if (c != EOF && c != '\n')
	r = r | c;

  if (r==0157150) c = tread(ohead);	/* oh */
  if (r==0163160) {			/* sp */
	nread(&o_sp);
	writebreak();
  }
  if (r==0164141) do_ta();		/* ta */
  if (r==0165154) nread(&o_ul);		/* ul */
  
  while (c != EOF && c != '\n') c = suck();
  return c != EOF;
}

int tread(char *s)
{
  int leadbl;
  leadbl = 0;
  while(1) {
	c = suck();
	if (c == ' ' && !leadbl) continue;
	if (c == EOF || c == '\n') {
		*s = '\0';
		return c;
	}
	*s = c; s++;
	leadbl++;
  }
}

void nread(int *i)
{
  int f;
  f = 0;
  *i = 0;
  if (!skipsp()) while(1) {
		c = suck();
		if (c == EOF) break;
		if (isspace(c)) break;
		if (isdigit(c)) {
			f++;
			*i = *i * 10 + c - '0';
		} else
			break;
	}
  if (!f) *i = 1;
}




void do_ta()
{
  int v;
  n_ta = 0;
  while(1) {
	nread(&v);
	if (v == 1)
		return;
	else {
		o_ta[n_ta] = v; n_ta=n_ta+1;
	}
	if (c == '\n' || c == EOF) break;
  }
}


int skipsp()
{
  while(1) {
	c= suck();
	if (c==EOF || c=='\n')	return(1);
	if (c==' ' || c=='\t')	continue;
	cachec= c;		/* Unget the character */
	return(0);
  }
}

void writebreak()
{
  int q;
  if (assylen) writeline(0, 1);
  q = TXTLEN;
  if (o_bl) {
	if (o_bl + line_no > q) {
		endpage();
		beginpage();
	}
	for (; o_bl; o_bl=o_bl - 1) blankline();
  } else if (o_sp) {
	if (o_sp + line_no > q)
		newpage();
	else if (line_no)
		for (; o_sp; o_sp= o_sp - 1) blankline();
  }
}

void blankline()
{
  if (line_no >= TXTLEN) newpage();
  if (o_n2) o_n2= o_n2 + 1;
  spit('\n');
  line_no= line_no + 1;
}

void writeline(int adflag, int flushflag)
{
  int j; int q;
  for (j = assylen - 1; j; j--) {
	if (assyline[j] == ' ')
		assylen=assylen - 1;
	else
		break;
  }
  q = TXTLEN;
  if (line_no >= q) newpage();
  for (j = 0; j < o_po; j++) spit(' ');
  if (o_n1) {
	if (o_nn) for (j = 0; j < o_ni + 4; j++)
			spit(' ');
	else {
		for (j = 0; j < o_ni; j++) spit(' ');
		spits(itoa(line_no + 1));
	}
  }
  if (o_n2) {
	if (o_nn) for (j = 0; j < o_ni + 4; j++)
			spit(' ');
	else {
		for (j = 0; j < o_ni; j++) spit(' ');
		spits(itoa(o_n2)); o_n2=o_n2 + 1;
	}
  }
  if (o_nn) o_nn= o_nn - 1;
  if (o_ce) for (j = 0; j < (o_ll - assylen + 1) / 2; j++)
		spit(' ');
  else
	for (j = 0; j < o_in; j++) spit(' ');
  if (adflag && !flushflag) fillline();
  for (j = 0; j < assylen; j++) spit(assyline[j]);
  spit('\n');
  assylen = 0;
  assyline[0] = 0;
  line_no= line_no + 1;
  for (j = 1; j < o_ls; j++)
	if (line_no <= q) blankline();
  if (!flushflag) {
	if (o_hc) dehyph(holdword);
	strcpy(assyline, holdword);
	assylen = strlen(holdword);
	*holdword = 0;
	holdp = holdword;
  }
  if (o_ix >= 0) o_in = o_ix;
  o_ix = -1; o_ti = -1;
}

void fillline()
{
  int excess; int j; int s; int inc; int spaces;
  adjtoggle = adjtoggle ^ 1;
  if (!(excess = o_ll - o_in - assylen)) return;
  if (excess < 0) {
	spits("roff: internal error #2\n");
	exit(1);
  }
  for (j = 2;; j++) {
	if (adjtoggle) {
		s = 0;
		inc = 1;
	} else {
		s = assylen - 1;
		inc = -1;
	}
	spaces = 0;
	while (s >= 0 && s < assylen) {
		if (assyline[s] == ' ')
			spaces++;
		else {
			if (0 < spaces && spaces < j) {
				insrt(s - inc);
				if (inc > 0) s++;
				excess--;
				if (!excess) return;
			}
			spaces = 0;
		}
		s = s + inc;
	}
  }
}

void insrt(int p)
{
  int i;
  for (i = assylen; i > p; i--) assyline[i] = assyline[i - 1];
  assylen= assylen + 1;
}

void newpage()
{
  if (page_no >= 0)
	endpage();
  else
	page_no = 1;
  beginpage();
}

void beginpage()
{
  int i;
  for (i = 0; i < o_m1; i++) spit('\n');
  if (page_no & 1) 
  	writetitle(ohead);
  else
  	writetitle(ehead);
  for (i = 0; i < o_m2; i++) spit('\n');
  line_no = 0;
}

void endpage()
{
  int i;
  for (i = line_no; i < TXTLEN; i++) blankline();
  for (i = 0; i < o_m3; i++) spit('\n');
  if (page_no & 1) 
  	writetitle(ofoot);
  else
  	writetitle(efoot);
  for (i = 0; i < o_m4; i++) spit('\n');
  if (o_bp < 0)
	page_no= page_no + 1;
  else {
	page_no = o_bp;
	o_bp = -1;
  }
}

void blankpage()
{
  int i;
  for (i = 0; i < o_m1; i++) spit('\n');
  if (page_no & 1) 
  	writetitle(ohead);
  else
  	writetitle(ehead);
  for (i = 0; i < o_m2; i++) spit('\n');
  for (i = 0; i < TXTLEN; i++) spit('\n');
  for (i = 0; i < o_m3; i++) spit('\n');
  if (page_no & 1) 
  	writetitle(ofoot);
  else
  	writetitle(efoot);
  page_no= page_no + 1;
  for (i = 0; i < o_m4; i++) spit('\n');
  line_no = 0;
}


void writetitle(char *t)
{
  char d; char *pst;
  int j; int l; int m; int n;
  int pstlen;

  d = *t;
  if (o_hx || !d) {
	spit('\n');
	return;
  }
  pst = pgform();
  pstlen= strlen(pst);
  for (j = 0; j < o_po; j++) spit(' ');
  t++;
  l = titlen(t, d, pstlen);
  while (*t && *t != d) {
	if (*t == '%')
		spits(pst);
	else
		spit(*t);
	t++;
  }
  if (!*t) {
	spit('\n');
	return;
  }
  t++;
  m = titlen(t, d, pstlen);
  for (j = l; j < (o_ll - m) / 2; j++) spit(' ');
  while (*t && *t != d) {
	if (*t == '%')
		spits(pst);
	else
		spit(*t);
	t++;
  }
  if (!*t) {
	spit('\n');
	return;
  }
  if ((o_ll - m) / 2 > l)
	m = m + (o_ll - m) / 2;
  else
	m = m + l;
  t++;
  n = titlen(t, d, pstlen);
  for (j = m; j < o_ll - n; j++) spit(' ');
  while (*t && *t != d) {
	if (*t == '%')
		spits(pst);
	else
		spit(*t);
	t++;
  }
  spit('\n');
}

char *pgform()
{
  char *pst;
  pst= itoa(page_no);
  return pst;
}

int titlen(char *t, char c, int k)
{
  int q;
  q = 0;
  while (*t && (*t != c)) {
	if (*t == '%')
        	q= q + k;
      	else
        	q++;
      	t++;
  }
  return q;
}

void spits(char *s)
{
  while (*s) {
	spit(*s); s++;
  }
}

void spit(char c)
{
  int ulflag;
  char *t;
  ulflag = c & UNDERL;
  c = c & ~UNDERL;
	
  if (page_no < startpage || (stoppage && page_no > stoppage)) return;
  if (c != ' ' && c != '\n' && n_blanks) {
	if (hflag && n_blanks > 1)
		while (col_no / 8 < (col_no + n_blanks) / 8) {
			putchar('\t');
			n_blanks = n_blanks - (8 - (col_no & 07));
			col_no = (8 + col_no) & ~07;
		}
	for (; n_blanks; n_blanks=n_blanks - 1) {
		putchar(' ');
		col_no= col_no + 1;
	}
  }
  if (ulflag && isalnum(c)) {
	putchar('_'); putchar(010);	/* _\b */
  }
  if (c == ' ')
	n_blanks=n_blanks + 1;
  else {
	putchar(c);
	col_no= col_no + 1;
  }
  if (c == '\n') {
	col_no = 0;
	n_blanks = 0;
  }
}

int suck()
{
  while(1) {
	c = cachec; cachec=0;	/* Get the cached char first */
	if (c==0) c = getchar();
	if (islegal(c)) return c;
  }
}


/*
 *	isspace, isalnum, isdigit, islegal - classify a character.
 *	We could just as well use <ctype.h> if it didn't vary from
 *	one version of Unix to another.  As it is, these routines
 *	must be modified for weird character sets, like EBCDIC and
 *	CDC Scientific.
 */

int isspace(int c)
{
  return(c== ' ' || c=='\t' || c=='\n');
}

int isalnum(int c)
{
  return(c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || (c >= '0' && c <= '9');
}

int isdigit(int c)
{
  return c >= '0' && c <= '9';
}

int islegal(int c)
{
  return (c >= ' ' && c <= '~') || isspace(c) || c == '\n' || c == EOF;
}

/* Given a number, convert the number
 * into ASCII digits and store in the
 * printbuf array. Null terminate the
 * string. Return a pointer to the first
 * digit in the character.
 */
char *itoa(int n)
{
  char *digitptr;
  digitptr= tempbuf + 9;        /* i.e digiptr= &tempbuf[9] */
  *digitptr= 0;
  digitptr--;

  while (n>0) {
    *digitptr = (n%10) + '0';   /* Store a digit */
    digitptr--;
    n=n/10;
  }

  return(digitptr+1);
}

#ifdef A_LANG
void strcpy(char *dst, char *src)
{
  while (*src) {
    *dst= *src;
    dst++; src++;
  }
  *dst=0;
}

int strlen(char *s)
{
  int n; n=0;
  while (*s) {
    s++; n++;
  }
  return(n);
}
#else
void printn(int n)
{
  printf("%d\n",n);
}
#endif
