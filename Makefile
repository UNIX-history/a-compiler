all: a.pm

a.pm: a.eyp
	eyapp -C a.eyp

hello.s: hello.a a.pm
	./a.pm hello.a

roff.s: roff.c a.pm
	./a.pm roff.c

run: hello.s
	./vm hello.s

roff: roff.c
	cc -o roff -g roff.c

rtest: roff
	( cd tests; ./rofftest)

test: a.pm
	( cd tests; ./runtests)

cmp: roff roff.s
	( cd tests; ./cmproff)

clean:
	rm -f a.pm *.s *.output *.out roff
