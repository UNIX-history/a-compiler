#!/usr/bin/perl
#
# a7out: user-mode simulator for PDP-7 Unix applications
#
# (c) 2016 Warren Toomey, GPL3
#
use strict;
use warnings;
use Data::Dumper;
use Term::ReadLine;

### Global variables ###
my $coverage   = 0;    # Code coverage flag
my $debug      = 0;    # Debug flag
my $singlestep = 0;    # Are we running in single-step mode?
my %Breakpoint;        # Hash of defined breakpoints
my @Mem;               # 8K 18-bit words of main memory
my %Addr2Name;	       # Map addresses to names
my %Addr2Label;	       # Map addresses to labels
my %Name2Addr;	       # Map names to addresses
my %Local;	       # Hash of local variables & registers
my $Current_fn="";     # Name of the function we are in
my @XCount;	       # Count of instructions executed per loc'n

# Registers
my $PC   = 0;          # Program counter
my $SP   = 0;          # Stack pointer

# Constants
use constant MAXINT    => 0777777;     # Biggest unsigned integer
use constant MAXADDR   => 017777;      # Largest memory address
use constant EPSILON   => 99;          # Max delta for symbol display

### Main program ###

# Get any optional arguments
while ( defined( $ARGV[0] ) && ( $ARGV[0] =~ m{^-} ) ) {

    # -d: debug mode
    if ( $ARGV[0] eq "-d" ) {
        $debug = 1;
        shift(@ARGV);
    }

    # -c: code coverage
    if ( $ARGV[0] eq "-c" ) {
        $coverage = 1;
        shift(@ARGV);
    }

    # -b: set a breakpoint
    if ( $ARGV[0] eq "-b" ) {
        shift(@ARGV);
        $Breakpoint{ lookup( shift(@ARGV) ) } = 1;
    }
}

# Check the arguments
die("Usage: $0 [-c] [-d] [-b breakpoint]  sfile\n")
  if ( @ARGV < 1 );

# Load the a.out file into memory
# and simulate it
load_code( $ARGV[0] );
simulate();
show_coverage() if ($coverage);
exit(0);

### Load the a.out file into memory
sub load_code {
    my $filename = shift;
    open(my $IN, "<", $filename)||die("Can't open $filename: $!\n");

    # Fill all the 8K words in memory with zeroes
    foreach my $i ( 0 .. MAXADDR ) {
        $Mem[$i] = 0;
    }

    my $addr=0;
    while (<$IN>) {
	chomp;
	if (m{^\t+# local (.*)$}) {	# Local var definitions
	    my ($fn, $name, $reg)=split(/:/, $1);
	    $Local{$fn}->{$name}= $reg;
	    next;
	}
	next if (m{^\t+#});		# Skip comment lines
	if (m{^(\S+):}) {		# A label
	    my $label= $1;
	    $Name2Addr{$label}= $addr;
	    if ($label=~ m{^LL}) {
	        $Addr2Label{$addr}= $label;
	    } else {
	        $Addr2Name{$addr}= $label;
	    }
	    next;
	}
	if (m{^\t*[A-Z]}) {
		s{\t+}{}g;
		$Mem[$addr++]= $_;	# An instruction
		next;
	}
	if (m{^\t*(-?\d+)}) {
		$Mem[$addr++]= $1;	# A literal value
		next;
	}
	die("Unrecognised input line: $_\n");
    }
    close($IN);
    #print(Dumper(\%Name2Addr));
    #print(Dumper(\%Local));
}

### Simulate the machine code loaded into memory
sub simulate {

    # List of opcodes that we can simulate
    my %Oplist = (
        'MOVSP' => \&movsp_inst,
        'CALL' => \&call_inst,
        'SYSCALL' => \&syscall_inst,
        'HLT' => \&hlt_inst,
        'LOAD' => \&load_inst,
        'STORE' => \&store_inst,
        'RET' => \&ret_inst,
        'MOV' => \&mov_inst,
        'ADD' => \&add_inst,
        'SUB' => \&sub_inst,
        'MUL' => \&mul_inst,
        'DIV' => \&div_inst,
        'MOD' => \&mod_inst,
        'AND' => \&and_inst,
        'OR' =>  \&or_inst,
        'XOR' => \&xor_inst,
        'LAND' => \&land_inst,
        'LOR' => \&lor_inst,
        'BEQ' => \&beq_inst,
        'BNE' => \&bne_inst,
        'CLT' => \&clt_inst,
        'CGT' => \&cgt_inst,
        'CLE' => \&cle_inst,
        'CGE' => \&cge_inst,
        'CEQ' => \&ceq_inst,
        'CNE' => \&cne_inst,
        'JMP' => \&jmp_inst,
        'INC' => \&inc_inst,
        'SHL' => \&shl_inst,
        'SHR' => \&shr_inst,
        'LOADA' => \&loada_inst,
        'MOVA' => \&mova_inst,
        'MOVIND' => \&movind_inst,
        'MOVTOIND' => \&movtoind_inst,
        'LOADAT' => \&loadat_inst,
        'STOREAT' => \&storeat_inst,
        'CMPL' => \&cmpl_inst,
        'NOT' => \&not_inst,
        'NEG' => \&neg_inst,
    );

    while (1) {
	#dprintf("Fetching instruction at PC $PC\n");
        # Get the instruction as an opcode and arguments. Lose tabs
	die("Out of bounds\n") if ($PC>MAXINT);
        my $instr= $Mem[$PC];
	die("no instruction at PC $PC\n") if (!defined($instr));
        #$instr=~ s{^\t+}{};
        my ($opcode, @args)= split(/ /, $instr);
        die("Unknown opcode $opcode at pc $PC\n")
				if (!defined($Oplist{$opcode}));

        # If this is a breakpoint, stop now and get a user command
        if ( defined( $Breakpoint{$PC} ) ) {
            $singlestep = 1;
            dprintf( "break at PC %s\n", addr($PC) );
        }
        get_user_command() if ($singlestep);

	# dprintf -> printf to avoid calling addr()
	printf("%s:\t$instr\n", addr($PC))
				if ( ($debug) || ($singlestep));
	$XCount[$PC]++;
	$Oplist{$opcode}->(@args);
        $PC++;
    }
}

# Set the SP to the specified address
sub movsp_inst
{
    my $label= shift;
    my $addr= $Name2Addr{$label};
    die("undefined label $label\n") if (!defined($addr));
    #dprintf("Set SP to $addr for $label\n");
    $SP= $addr;
}

# Increment the SP, jump to the given label and
# save the old PC on the stack
sub call_inst
{
    my ($increment, $label)= @_;
    #dprintf("Before the call, PC is %d, SP is %d\n", $PC, $SP);
    #dprintf("Incrementing SP by $increment\n");
    #dprintf("  saving old SP %d at location %d\n", $SP, $SP + $increment);

    $Mem[ $SP + $increment ] = $SP;
    $SP += $increment;

    # Save old PC on the stack
    $Mem[ $SP+1 ] = $PC;
   
    $Current_fn= $label;
    my $addr= $Name2Addr{$label};
    die("undefined label $label\n") if (!defined($addr));
    #dprintf("Set PC to $addr for $label\n");
    #dprintf("Setting memory %d to old PC %d\n", $SP+1, $PC);
    $PC= $addr-1;
}

# Pretty much the same as call_inst
sub syscall_inst
{
    my ($increment, $label)= @_;
    #dprintf("Before the call, PC is %d, SP is %d\n", $PC, $SP);
    #dprintf("Incrementing SP by $increment\n");
    #dprintf("  saving old SP %d at location %d\n", $SP, $SP + $increment);

    $Mem[ $SP + $increment ] = $SP;
    $SP += $increment;

    # Save old PC on the stack
    $Mem[ $SP+1 ] = $PC;
   
    # Deal with printn
    if ($label eq "printn") {
	print($Mem[ $SP + 2 ], "\n");
	ret_inst();
	return;
    }

    # Deal with putchar
    if ($label eq "putchar") {
	print(chr($Mem[ $SP + 2 ]));
	ret_inst();
	return;
    }

    # Deal with getchar
    if ($label eq "getchar") {
        my $ch= getc(STDIN);
	my $val= (defined($ch)) ? ord($ch) : 4;		# 4 is EOF
	$Mem[$SP + 3] = $val;
	#dprintf("getchar got character %d\n", $val);
	ret_inst(3);
	return;
    }

    # Deal with exit
    if ($label eq "exit") {
	show_coverage() if ($coverage);
	exit($Mem[ $SP + 2 ]);
    }
}

sub hlt_inst
{
	dprintf("Halting\n");
	show_coverage() if ($coverage);
	exit(0);
}

# Load the value at an address
sub load_inst
{
    my ($reg, $value)= @_;
    #dprintf("Setting $reg to value at location $value\n");
    $reg=~ s{^r}{};
    # value is either a number or an identifier
    $value= $Name2Addr{$value} if (defined($Name2Addr{$value}));
    $Mem[ $SP + $reg] = $Mem[ $value];
}

# Load a literal value or an address
sub loada_inst
{
    my ($reg, $value)= @_;
    #dprintf("Setting $reg to literal value $value\n");
    $reg=~ s{^r}{};
    # value is either a number or an identifier
    $value= $Name2Addr{$value} if (defined($Name2Addr{$value}));
    $Mem[ $SP + $reg] = $value;
}

# Get back the old SP and the old PC
sub ret_inst
{
    my $reg= shift;
    my $retval;
    if (defined($reg)) {
      $reg=~ s{^r}{};
      $retval= $Mem[ $SP + $reg];
      #dprintf("Returning value %d\n", $retval);
    } else {
      $retval= 0;
      #dprintf("Returning no value\n");
    }
    #dprintf("  got PC %d from locn %d, SP %d from %d\n",
    #	$PC, $SP+1, $Mem[ $SP ], $SP);
    $PC= $Mem[ $SP + 1];
    my $temp= $SP;
    $SP= $Mem[ $SP ];
    $Mem[ $temp] = $retval;
}

sub mov_inst
{
    my ($reg1, $reg2)= @_;
    $reg1=~ s{^r}{};
    $reg2=~ s{^r}{};
    #dprintf("Copying r%d %d to r%d\n", $reg2, $Mem[ $SP + $reg2], $reg1);
    $Mem[ $SP + $reg1] = $Mem[ $SP + $reg2];
}

sub add_inst
{
    my ($reg1, $reg2, $reg3)= @_;
    #dprintf("$reg1= $reg2 + $reg3\n");
    $reg1=~ s{^r}{};
    $reg2=~ s{^r}{};
    $reg3=~ s{^r}{};
    $Mem[ $SP + $reg1] = $Mem[ $SP + $reg2] + $Mem[ $SP + $reg3];
}

sub mul_inst
{
    my ($reg1, $reg2, $reg3)= @_;
    #dprintf("$reg1= $reg2 * $reg3\n");
    $reg1=~ s{^r}{};
    $reg2=~ s{^r}{};
    $reg3=~ s{^r}{};
    $Mem[ $SP + $reg1] = $Mem[ $SP + $reg2] * $Mem[ $SP + $reg3];
}

sub div_inst
{
    my ($reg1, $reg2, $reg3)= @_;
    #dprintf("$reg1= $reg2 / $reg3\n");
    $reg1=~ s{^r}{};
    $reg2=~ s{^r}{};
    $reg3=~ s{^r}{};
    {
      use integer;
      $Mem[ $SP + $reg1] = $Mem[ $SP + $reg2] / $Mem[ $SP + $reg3];
    }
}

sub mod_inst
{
    my ($reg1, $reg2, $reg3)= @_;
    #dprintf("$reg1= $reg2 mod $reg3\n");
    $reg1=~ s{^r}{};
    $reg2=~ s{^r}{};
    $reg3=~ s{^r}{};
    $Mem[ $SP + $reg1] = $Mem[ $SP + $reg2] % $Mem[ $SP + $reg3];
}

sub sub_inst
{
    my ($reg1, $reg2, $reg3)= @_;
    #dprintf("$reg1= $reg2 - $reg3\n");
    $reg1=~ s{^r}{};
    $reg2=~ s{^r}{};
    $reg3=~ s{^r}{};
    $Mem[ $SP + $reg1] = $Mem[ $SP + $reg2] - $Mem[ $SP + $reg3];
    #dprintf("  sub %d - %d => %d\n", $Mem[ $SP + $reg2], $Mem[ $SP + $reg3],
    #		$Mem[ $SP + $reg1]);
}

sub and_inst
{
    my ($reg1, $reg2, $reg3)= @_;
    #dprintf("$reg1= $reg2 \& $reg3\n");
    $reg1=~ s{^r}{};
    $reg2=~ s{^r}{};
    $reg3=~ s{^r}{};
    $Mem[ $SP + $reg1] = $Mem[ $SP + $reg2] & $Mem[ $SP + $reg3];
    #dprintf("  0%o & 0%o => 0%o\n", $Mem[ $SP + $reg2],
    #		$Mem[ $SP + $reg3], $Mem[ $SP + $reg1]);
}

sub or_inst
{
    my ($reg1, $reg2, $reg3)= @_;
    #dprintf("$reg1= $reg2 | $reg3\n");
    $reg1=~ s{^r}{};
    $reg2=~ s{^r}{};
    $reg3=~ s{^r}{};
    $Mem[ $SP + $reg1] = int($Mem[ $SP + $reg2]) | int($Mem[ $SP + $reg3]);
}

sub xor_inst
{
    my ($reg1, $reg2, $reg3)= @_;
    #dprintf("$reg1= $reg2 ^ $reg3\n");
    $reg1=~ s{^r}{};
    $reg2=~ s{^r}{};
    $reg3=~ s{^r}{};
    $Mem[ $SP + $reg1] = int($Mem[ $SP + $reg2]) ^ int($Mem[ $SP + $reg3]);
}

# 1 is true, 0 otherwise
sub bool
{
  my $x= shift;
  return(($x==0) ? 0 : 1);
}

sub land_inst
{
    my ($reg1, $reg2, $reg3)= @_;
    #dprintf("$reg1= $reg2 && $reg3\n");
    $reg1=~ s{^r}{};
    $reg2=~ s{^r}{};
    $reg3=~ s{^r}{};
    $Mem[ $SP + $reg1] = bool($Mem[ $SP + $reg2]) & bool($Mem[ $SP + $reg3]);
    #dprintf("    land %d && %d => %d\n", bool($Mem[ $SP + $reg2]),
    #		bool($Mem[ $SP + $reg3]), $Mem[ $SP + $reg1]);
}

sub lor_inst
{
    my ($reg1, $reg2, $reg3)= @_;
    #dprintf("$reg1= $reg2 || $reg3\n");
    $reg1=~ s{^r}{};
    $reg2=~ s{^r}{};
    $reg3=~ s{^r}{};
    $Mem[ $SP + $reg1] = bool($Mem[ $SP + $reg2]) | bool($Mem[ $SP + $reg3]);
}

sub beq_inst
{
    my ($reg, $label)= @_;
    $reg=~ s{^r}{};
    #dprintf("In beq with r%s and $label, looking at %s\n",$reg,
    #	$Mem[$SP + $reg]);
    my $addr= $Name2Addr{$label};
    die("undefined label $label\n") if (!defined($addr));
    if ($Mem[$SP + $reg] == 0) {
        #dprintf("  beq jumping to $addr $label\n");
        $PC= $addr-1;
    }
}

sub bne_inst
{
    my ($reg, $label)= @_;
    $reg=~ s{^r}{};
    #dprintf("In bne with r%s and $label, looking at %s\n",$reg,
    #	$Mem[$SP + $reg]);
    my $addr= $Name2Addr{$label};
    die("undefined label $label\n") if (!defined($addr));
    if ($Mem[$SP + $reg] != 0) {
        #dprintf("  bne jumping to $addr $label\n");
        $PC= $addr-1;
    }
}

# Compare less than. reg1= (r2 < r3) ? 1 : 0
sub clt_inst
{
    my ($reg1, $reg2, $reg3)= @_;
    #dprintf("$reg1= $reg2 < $reg3\n");
    $reg1=~ s{^r}{}; $reg2=~ s{^r}{}; $reg3=~ s{^r}{};
    $Mem[ $SP + $reg1] = ($Mem[ $SP + $reg2] < $Mem[ $SP + $reg3]) ? 1 : 0;
}

sub cgt_inst
{
    my ($reg1, $reg2, $reg3)= @_;
    #dprintf("$reg1= $reg2 > $reg3\n");
    $reg1=~ s{^r}{}; $reg2=~ s{^r}{}; $reg3=~ s{^r}{};
    $Mem[ $SP + $reg1] = ($Mem[ $SP + $reg2] > $Mem[ $SP + $reg3]) ? 1 : 0;
}

sub cle_inst
{
    my ($reg1, $reg2, $reg3)= @_;
    #dprintf("$reg1= $reg2 <= $reg3\n");
    $reg1=~ s{^r}{}; $reg2=~ s{^r}{}; $reg3=~ s{^r}{};
    $Mem[ $SP + $reg1] = ($Mem[ $SP + $reg2] <= $Mem[ $SP + $reg3]) ? 1 : 0;
}

sub cge_inst
{
    my ($reg1, $reg2, $reg3)= @_;
    #dprintf("$reg1= $reg2 >= $reg3\n");
    $reg1=~ s{^r}{}; $reg2=~ s{^r}{}; $reg3=~ s{^r}{};
    $Mem[ $SP + $reg1] = ($Mem[ $SP + $reg2] >= $Mem[ $SP + $reg3]) ? 1 : 0;
}

sub ceq_inst
{
    my ($reg1, $reg2, $reg3)= @_;
    #dprintf("$reg1= $reg2 == $reg3\n");
    $reg1=~ s{^r}{}; $reg2=~ s{^r}{}; $reg3=~ s{^r}{};
    $Mem[ $SP + $reg1] = ($Mem[ $SP + $reg2] == $Mem[ $SP + $reg3]) ? 1 : 0;
}

sub cne_inst
{
    my ($reg1, $reg2, $reg3)= @_;
    #dprintf("$reg1= $reg2 != $reg3\n");
    $reg1=~ s{^r}{}; $reg2=~ s{^r}{}; $reg3=~ s{^r}{};
    $Mem[ $SP + $reg1] = ($Mem[ $SP + $reg2] != $Mem[ $SP + $reg3]) ? 1 : 0;
}

sub jmp_inst
{
    my $label= shift;
    my $addr= $Name2Addr{$label};
    die("undefined label $label\n") if (!defined($addr));
    #dprintf("Jumping to $addr $label\n");
    $PC= $addr-1;
}

sub shl_inst
{
    my ($reg1, $reg2, $reg3)= @_;
    #dprintf("$reg1= $reg2 << $reg3\n");
    $reg1=~ s{^r}{};
    $reg2=~ s{^r}{};
    $reg3=~ s{^r}{};
    $Mem[ $SP + $reg1] = $Mem[ $SP + $reg2] << $Mem[ $SP + $reg3];
}

sub shr_inst
{
    my ($reg1, $reg2, $reg3)= @_;
    #dprintf("$reg1= $reg2 >> $reg3\n");
    $reg1=~ s{^r}{};
    $reg2=~ s{^r}{};
    $reg3=~ s{^r}{};
    $Mem[ $SP + $reg1] = $Mem[ $SP + $reg2] >> $Mem[ $SP + $reg3];
}

sub inc_inst
{
    my ($reg, $value)= @_;
    #dprintf("$reg += $value\n");
    $reg=~ s{^r}{};
    $Mem[$SP + $reg] += $value;
}

sub mova_inst
{
    my ($reg1, $reg2)= @_;
    #dprintf("mova copying &$reg2 to $reg1\n");
    $reg1=~ s{^r}{};
    $reg2=~ s{^r}{};
    $Mem[ $SP + $reg1] = $SP + $reg2;
}

sub movind_inst
{
    my ($reg1, $reg2)= @_;
    $reg1=~ s{^r}{};
    $reg2=~ s{^r}{};
    $Mem[$SP + $reg1]= $Mem[ $Mem[$SP + $reg2] ];
    #dprintf("Moved value %s into r%s from indirect r%s\n",
    #		$Mem[$SP + $reg1], $reg1, $reg2);
}

sub movtoind_inst
{
    my ($reg1, $reg2)= @_;
    $reg1=~ s{^r}{};
    $reg2=~ s{^r}{};
    $Mem[ $Mem[$SP + $reg1] ]= $Mem[$SP + $reg2];
    #dprintf("Moved value %s from r%s into indirect r%s addr %s\n",
    #		$Mem[$SP + $reg2], $reg2, $reg1, $Mem[$SP + $reg1]);
}

sub loadat_inst
{
    my ($reg, $value)= @_;
    #dprintf("Loading $reg through pointer at location $value\n");
    $reg=~ s{^r}{};
    # value is either a number or an identifier
    $value= $Name2Addr{$value} if (defined($Name2Addr{$value}));
    $Mem[ $SP + $reg] = $Mem[ $Mem[$value] ] ;
}


# Store the value at an address
sub store_inst
{
    my ($reg, $value)= @_;
    #dprintf("Storing $reg into location $value\n");
    $reg=~ s{^r}{};
    # value is either a number or an identifier
    $value= $Name2Addr{$value} if (defined($Name2Addr{$value}));
    $Mem[ $value] = $Mem[ $SP + $reg];
}

# Store the value through the pointer at an address
sub storeat_inst
{
    my ($reg, $value)= @_;
    #dprintf("Storing $reg through pointer at location $value\n");
    $reg=~ s{^r}{};
    # value is either a number or an identifier
    $value= $Name2Addr{$value} if (defined($Name2Addr{$value}));
    $Mem[ $Mem[$value] ] = $Mem[ $SP + $reg];
}

# Complement a register
sub cmpl_inst
{
    my ($reg1, $reg2)= @_;
    $reg1=~ s{^r}{};
    $reg2=~ s{^r}{};
    #dprintf("CMPL $reg2, was 0%o ", $Mem[ $SP + $reg2]);
    $Mem[ $SP + $reg1] = ($Mem[ $SP + $reg2] ^ 0777777) & 0777777;
    #dprintf("now 0%o\n", $Mem[ $SP + $reg1]);
}

# Logical not a register
sub not_inst
{
    my ($reg1, $reg2)= @_;
    $reg1=~ s{^r}{};
    $reg2=~ s{^r}{};
    #dprintf("NOT $reg2, was %d ", $Mem[ $SP + $reg2]);
    $Mem[ $SP + $reg1] = $Mem[ $SP + $reg2] ? 0 : 1;
    #dprintf("now %d\n", $Mem[ $SP + $reg1]);
}

# Negate a value
sub neg_inst
{
    my ($reg1, $reg2)= @_;
    $reg1=~ s{^r}{};
    $reg2=~ s{^r}{};
    #dprintf("NEG $reg2, was %d ", $Mem[ $SP + $reg2]);
    $Mem[ $SP + $reg1] = - $Mem[ $SP + $reg2];
    #dprintf("now %d\n", $Mem[ $SP + $reg1]);
}


# Debug code: dump memory contents
# Print from $start to $end.
# Print empty locations if $yeszero
sub dump_memory {
    my ( $start, $end, $yeszero ) = @_;

    # Special case: register
    if ($start=~ m{^r(\d+)$}) {
      my $i= $1;
      my $c= ($Mem[$SP+$i] && ($Mem[$SP+$i]<0200)) ? chr($Mem[$SP+$i]) : '';
      printf("r%d: %s\t%s\n", $i, $Mem[$SP+$i], $c);
      return;
    }
    
    foreach my $i ( $start .. $end ) {

        # Convert the word into possibly two ASCII characters
	my ($c1, $c2)=(" ", " ");
	if ($Mem[$i]=~ m{^\d+$}) {
          $c1 = ( $Mem[$i] >> 9 ) & 0777;
          $c1 = ( $c1 < 0200 ) ? chr($c1) : " ";
          $c2 = $Mem[$i] & 0777;
          $c2 = ( $c2 < 0200 ) ? chr($c2) : " ";
	}
	my $label= (defined($Addr2Name{$i})) ? $Addr2Name{$i} : $i;
	$label= (defined($Addr2Label{$label})) ? $Addr2Label{$label} : $label;
        printf( STDERR "%s: %s %s%s\n", $label, $Mem[$i], $c1, $c2 )
          if ( $yeszero || $Mem[$i] );
    }
}

# Show code coverage, similar to dump
sub show_coverage
{
  my $start=0;
  my $end= $Name2Addr{'LLend'};
  foreach my $i ( $start .. $end ) {
    my $count= $XCount[$i] || "";
    my $label= (defined($Addr2Name{$i})) ? $Addr2Name{$i} : $i;
    $label= (defined($Addr2Label{$label})) ? $Addr2Label{$label} : $label;
    printf("%s: %s\t%s\n", $label, $Mem[$i], $count);
  }
}

# Lookup a named address or a named address + offset
# If the argument is a register or a number, just return it
sub lookup
{
  my $x = shift;
  return ($x) if ($x =~ m/^r?\d+$/);

  return($Local{$Current_fn}->{$x})
                        if defined($Local{$Current_fn}->{$x});


  $x=~ s/\s//g;
  my ($name, $offset)= split(/\+/,$x);
  if (!defined($Name2Addr{$name})) {
    print("Unknown identifier $name\n"); return(0);
  }
  $offset=0 if (!defined($offset));
  return($Name2Addr{$name}+$offset);
}

### Format an address
sub addr {
    my $addr = shift;
    if (%Addr2Name) {
        return "$addr ($Addr2Name{$addr})" if ($Addr2Name{$addr});
        # XXX keep Addr2Name as a sorted array?
        # prefer after to before
        for (my $epsilon = 1; $epsilon <= EPSILON; $epsilon++) {
            my $n = $Addr2Name{$addr-$epsilon};
            return "$addr ($n+$epsilon)" if (defined($n));
        }
        for (my $epsilon = 1; $epsilon <= EPSILON; $epsilon++) {
            my $n = $Addr2Name{$addr+$epsilon};
            return "$addr ($n-$epsilon)" if (defined($n));
        }
    }
    return $addr;
}

# Print out debug messages
sub dprintf {
    #print Dumper(\@_);
    printf( STDERR @_ ) if ( ($debug) || ($singlestep) );
}

# Get one or more commands from the user and execute them
my $TTY;	# File handle for /dev/tty
sub get_user_command {
    my %Cmdlist = (
        'b'        => \&cmd_setbreak,
        'break'    => \&cmd_setbreak,
        'd'        => \&cmd_dump,
        'p'        => \&cmd_dump,
        'print'    => \&cmd_dump,
        'dump'     => \&cmd_dump,
        'db'       => \&cmd_delbreak,
        'del'      => \&cmd_delbreak,
        'delete'   => \&cmd_delbreak,
        '?'        => \&cmd_help,
        'h'        => \&cmd_help,
        'help'     => \&cmd_help,
        's'        => \&cmd_step,
        'l'        => \&cmd_listbreak,
        'list'     => \&cmd_listbreak,
        'step'     => \&cmd_step,
        'q'        => \&cmd_exit,
        'x'        => \&cmd_exit,
        'quit'     => \&cmd_exit,
        'exit'     => \&cmd_exit,
        'c'        => \&cmd_continue,
        'continue' => \&cmd_continue,
        'r'        => \&cmd_showregs,
        'regs'     => \&cmd_showregs,
    );

    # Open the terminal and the readline object
    if (!defined($TTY)) {
      open($TTY, "+<", "/dev/tty") || die("Can't open /dev/tty: $!\n");
    }
    my $term = new Term::ReadLine('AVM',$TTY,$TTY);
    $term->ornaments(0);

    # Loop until we get a leave result
    while (defined (my $line = $term->readline('prompt> ')) ) {

        # Get a command from the user
        # and split into command, start and end addresses.
        chomp($line);
        my ( $cmd, $addr, $endaddr ) = split( /\s+/, $line );

        # Convert addresses
        $addr    = lookup($addr)    if ( defined($addr) );
        $endaddr = lookup($endaddr) if ( defined($endaddr) );

        # Run the command
        my $leave;
        if ( defined($cmd) && defined( $Cmdlist{$cmd} ) ) {
            $leave = $Cmdlist{$cmd}->( $addr, $endaddr );
        }
        else {
            printf( "%s: unknown command\n", $cmd || "" );
            cmd_help();
        }
        return if ($leave);
    }
}

# Exit the program
sub cmd_exit {
    exit(0);
}

# Continue by disabling single-step
# and break out of the command loop
sub cmd_continue {
    $singlestep = 0;
    return (1);
}

# Step by staying in single-step
# but break out of the command loop
sub cmd_step {
    return (1);
}

# Set a breakpoint
sub cmd_setbreak {
    my $addr = shift;
    $Breakpoint{$addr} = 1;
    return (0);
}

# Delete a breakpoint
sub cmd_delbreak {
    my $addr = shift;
    delete( $Breakpoint{$addr} );
    printf( "Delete breakpoint %d\n", $addr );
    return (0);
}

sub cmd_help {
    print("  [b]reak address             set a breakpoint\n");
    print("  [c]ontinue                  leave single-step and continue\n");
    print("  [d]ump  [start] [end]       dump addresses in range\n");
    print("  [p]rint [start] [end]       dump addresses in range\n");
    print("  db address                  delete a breakpoint\n");
    print("  [del]ete address            delete a breakpoint\n");
    print("  [l]ist                      list breakpoints\n");
    print("  [r]egs                      print PC, SP and regs\n");
    print("  [s]tep                      single-step next instruction\n");
    print("  ?, h, help                  print this help list\n");
    print("  e[x]it, [q]uit              exit the program\n");
    return (0);
}

sub cmd_showregs {
    # Convert Local into a list of registers
    my %Reg;
    my $href= $Local{$Current_fn};
    if ($href) {
	foreach my $local (keys(%$href)) {
	    $Reg{ $href->{$local}}= $local;
	}
    }
    print("PC is $PC, SP is $SP\n");
    foreach my $i (2 .. 15) {
      my $ri= "r$i";
      print("$ri");
      print("($Reg{$ri})") if (defined($Reg{"$ri"}));
      print(":\t", $Mem[$SP+$i], "\n");
    }
    return (0);
}

sub cmd_dump {
    my ( $start, $end ) = @_;

    # No arguments, so dump everything but not empty locations
    if ( !defined($start) ) {
        dump_memory( 0, MAXADDR, 0 );
        return (0);
    }

    # If the start is a local variable, convert to a register
    $start= $Local{$Current_fn}->{$start}
			if defined($Local{$Current_fn}->{$start});

    # Dump a limited range
    $end = $start if ( !defined($end) );
    dump_memory( $start, $end, 1 );
    return (0);
}

sub cmd_listbreak {
    print("Breakpoints:\n");
    foreach my $addr ( sort( keys(%Breakpoint) ) ) {
        printf( "  %d\n", $addr );
    }
    return (0);
}
