"A" language Compiler
=====================

(This is now obsolete; you should look at https://github.com/DoctorWkt/h-compiler)

This is a one-pass compiler for a small subset of C. I've called the language
"A", mainly because B and C have already been taken. The original aim was
to create a language that was similar to C, but which could be compiled down
to a virtual machine assembly code which could squeeze onto PDP-7 Unix.

The language still looks very C-like, but there are some important
differences. Even though the language allows char and int, there is only
really int. This means that the '*' decoration on declarations does nothing,
and neither does 'void'. They are just syntactic sugar to make it a bit more
like C.

To build the compiler and run the virtual machine to execute the assembly code,
you need to have Perl, the Parse::Yapp, Term::ReadLine::Gnu and Data::Dumper
packages installed. You can then run "make test". This builds the compiler
(a.eyp -> a.pm), and runs the test suite of programs in the VM. The VM
executable is called vm.

Read the instructions file for ideas on the VM and the instruction set. If
you want to see the Yacc grammar of the language, do "eyapp -c a.eyp | less"

This is all heavily under development. Expect it to not work as expected :)
The change log follows below.

Cheers, Warren

Wed Apr 27 07:48:38 AEST 2016
-----------------------------
I've just got roff to compile, run and produce reasonable output. It's
not yet 100% identical to the C version, but it's quite close. Later:
now it's identical output.

Tue Apr 26 21:48:30 AEST 2016
-----------------------------
The compiler mostly works now and I have a suite of test cases so I can
see if I break things. The VM is pretty complete too, and I've added
lots of code to help with single-step, breakpoints and mem/reg dumps.

I ran a code coverage profile on the Minix roff.c, identified the sections
that we don't use, and trimmed back the code. Then I modified the roff.c
leftover code so that it is both C code and "A" code. It now compiles
down to 2,146 words of machine code and bss. That's great. Right now
it doesn't run correctly, but I will be able to single-step both the
A and C versions to see where they diverge.

Sun Apr 24 11:18:30 AEST 2016
-----------------------------
I've added ++ and -- on locals, << and >>. I've changed the register
allocation code. It's got to the point where I can start converting
the Minix roff.c into roff.a. I've made a start and the assembly code
is roughly half the number of output instructions as the B output.
I've also written some test programs so I can see when I break things.

Tue Apr 19 14:40:09 AEST 2016
-----------------------------

So far we have expressions, assignments, IF, IF/ELSE, WHILE, function calls
and returns, one-dimensional arrays but they have to be global.

Things missing: ++, --, += and friends. No generate code optimisation yet.
